# Sky Test Api

Este é um projeto de teste para alocação Accenture Sky

**Descrição do projeto**

Aplicação REST desenvolvida em Node Express que contempla as seguintes tecnologias:
1. Node Express
2. Jwt para autenticação.
3. MongoDB 
4. Mocha + Sinon modules para test unítário 
5. Eslint module para padronização de código
6. NYC module para test de cobertura de código
7. Lodash module para tratamentos de variáveis


**Funções**
1. Endpoint para criar usuário  
2. Logar e retornar token de acesso após a criação do usuário
3. Endpoind para login do usuário
4. Retornar token do usuário 


**Como instalar e executar:**

1. Clone o respositório utilizando o comando **git clone https://gitlab.com/ivan.lemos/sky-test-api.git**
2. Instale os pacotes da aplicação com o comando **"npm install"**
3. Para rodar a aplicação, execute o comando **"npm run dev"**

**Obs:** Não é necessário configurar uma instância do mongoDb pois já existe banco configurado para este projeto no mongodb.net/. Ao executar a aplicação, automaticamente será realizada a conexão e a aplicação será executada normalmente.  

**Teste unitário e cobertura de código:**

1. Ao realizar uma mudança no projeto, é necessário verificar ou criar testes unitário para cada nova funcionalidade. Ao efetuar o commit o sistema automaticamente efetua uma checagem de testes unitários e cobertura de códigos porém o mesmo só terá sucesso caso os testes retortem 100% de sucesso.  

2. Para realizar somente os testes unitários, execute o comando **"npm run debug"**

3. Para realizar os testes unitários e cobertura, execute o comando **"npm run test"**


********

**Endpoints** 

| Endpoint | Métodos | Descrição |
| ------ | ------ | ------ |
| **/api/auth/signup** | POST | Cadastra um usuário no sistema. Ao cadastrar e exibe o x-access-token da sessão criada |
| **/api/auth/signin** | POST | Efetua o login do usuário recebendo seu dados e  o x-access-token para autenticações |
| **/api/user**| GET | Realiza pesquisa do usuário através do id enviado no body da requisição. Obs: X-access-token obrigatório  |
| **/api/test/user**| GET | Método para teste unitário |
| **/api/test/all**| GET | Método para teste unitário |
| **/api/test/admin**| GET | Método para teste unitário |

********

**Collection POSTMAN** 

Na raiz da aplicação foi disponibilizado o arquivo **Sky - API REST - Developer Test.postman_collection.json** para importação no POSTMAN.
Todos os endpoints já estão configurandos recebendo automaticamente o x-access-token gerado na singin via variável de collection.

Foi configurado na collection as variávies **host_dev** (local) e  **host_prod** (produção) para que sejam utilizadas nos testes dos endpoints.

É possível realizar testes na aplicação sem realizar qualquer instalação apenas **importando a collection** e  apontando para o **host_prod** na url do endpoint.

********

**MONGO DB** 

Para visualizar os resultados, acesse o Mongo DB através da string de conexão **mongodb+srv://skydb:AszQ6WVRbUFjkKY@cluster0.llyeb.mongodb.net/skydb?retryWrites=true&w=majority**






_Ivan Silva Lemos - Digital Business Integration Consultant_
 





