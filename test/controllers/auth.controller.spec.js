const chai = require('chai')
const expect = chai.expect
const sinon = require('sinon'); 
const { hashSync } = require('bcryptjs');

const { user  } = require('../../app/models');
const controller = require('../../app/controllers/auth.controller');


describe("AUTH CONTROLLER", () => {

  beforeEach(() => {
    sinonSandBox = sinon.createSandbox();
  });

  afterEach(() => {
    // Restore all the things made through the sandbox
    sinonSandBox.restore();
  });
  
 
  it("1 - SINGNIN USER - Error test - Status 500   ", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    };
    sinonSandBox.stub(user, 'findOne').returns({
      populate : () => (
        {
          exec : (process) => {
            const err = 'error test';
            process(err);
          } ,
        }),
    });
    const result = controller.signin( req , res );
    expect(result).to.be.equal(undefined);
  })

  it("2 - SINGNIN USER - User not found - Status 404   ", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    };
    sinonSandBox.stub(user, 'findOne').returns({
      populate : () => (
        {
          exec : (process) => {
            const user = false;
            process(user);
          } ,
        }),
    });
    const result = controller.signin( req , res );
    expect(result).to.be.equal(undefined);
  })

  it("3 - SINGNIN USER - User password Not Valid  ", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    };
    
    sinonSandBox.stub(user, 'findOne').returns({
      populate : () => (
        {
          exec : (process) => {
            process(false,{password:'123456'});
          } ,
        }),
    });
    const result = controller.signin( req , res );
    expect(result).to.be.equal(undefined);
  })

  it("4 - SINGNIN USER - Erro on Save ", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    };
    
    sinonSandBox.stub(user, 'findOne').returns({
      populate : () => (
        {
          exec : (process) => {
            process(false,
              {
                id:'0002', 
                password: hashSync('accenture@12345', 8),
                roles: [{"$oid":"5f8fe8600cf5eb14bf74b442",name : "admin"}],
                lastLoginAt: null,
                save:(onSave) => onSave(true),
              });
          } ,
        }),
    });
    const result = controller.signin( req , res );
    expect(result).to.be.equal(undefined);
  })

  it("5 - SINGNIN USER - Success on Save ", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    };
    
    sinonSandBox.stub(user, 'findOne').returns({
      populate : () => (
        {
          exec : (process) => {
            process(false,
              {
                id:'0002', 
                password: hashSync('accenture@12345', 8),
                roles: [{"$oid":"5f8fe8600cf5eb14bf74b442",name : "admin"}],
                lastLoginAt: null,
                save:(onSave) => onSave(false),
              });
          } ,
        }),
    });
    const result = controller.signin( req , res );
    expect(result).to.be.equal(undefined);
  })


  it("6 - SIGNUP USER - Create NEW USER - StatusCode 500", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        phones: '+55988776655',
      },
      user: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        save: (process) =>  process(true,'error message'),   
      },
      
    };

    const result = controller.signup( req , res ); // error 500

    expect(result).to.be.equal(undefined);
  })

  it("7 - SIGNUP USER - Create NEW USER - Error find roles - StatusCode 500", ()=> {

    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        phones: '+55988776655',
        roles: ["user","moderator"],
      },
      user: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        save: (process) => process(false, null),
      },
      role: {
        find: (data, process) => process(true, null),
        findOne: (data, process) => process(false, null),
      },
      
    };
   
    const result = controller.signup( req , res ); // error 500
    expect(result).to.be.equal(undefined);
  })

  it("8 - SIGNUP USER - Create NEW USER - Success save user - StatusCode 500", ()=> {

    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        phones: '+55988776655',
        roles: ["user","moderator"],
      },
      user: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        save: (process) => process(false, {_id:'22333',
          save: (process) => process(false, {_id:'22333'}),
        }),
      },
      role: {
        find: (data, process) => process(false,[{_id:'22333'}]),
        findOne: (data, process) => process(false, null),
      },
      
    };
   
    const result = controller.signup( req , res ); // error 500
    expect(result).to.be.equal(undefined);
  })

  it("9 - SIGNUP USER - Create NEW USER - Error role in request - StatusCode 500", ()=> {

    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        phones: '+55988776655',
        roles: null,
      },
      user: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        save: (process) => process(false, {
          roles: null,
          save: (process) => process(false, {roles: null}),
        }),
      },
      role: {
        find: (data, process) => process(false, null),
        findOne: (data, process) => process(false, {_id: '11223'}),
      },
      
    };
   
    const result = controller.signup( req , res ); // error 500
    expect(result).to.be.equal(undefined);
  })

  it("10 - SIGNUP USER - Create NEW USER - Error role Find - StatusCode 500", ()=> {

    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        phones: '+55988776655',
        roles: 'admin',
      },
      user: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        save: (process) => process(false, {
          roles: 'null',
          save: (process) => process(false, {roles: null}),
        }),
      },
      role: {
        find: (data, process) => process(true, null),
        findOne: (data, process) => process(false, {_id: '11223'}),
      },
      
    };
   
    const result = controller.signup( req , res ); // error 500
    expect(result).to.be.equal(undefined);
  })

  it("11 - SIGNUP USER - Create NEW USER - Error userSave - StatusCode 500", ()=> {

    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        phones: '+55988776655',
        roles: 'admin',
      },
      user: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        save: (process) => process(false, {
          roles: 'null',
          save: (process) => process(true, {roles: null}),
        }),
      },
      role: {
        find: (data, process) => process(false, [{_id:'0001'}]),
        findOne: (data, process) => process(false, {_id: '11223'}),
      },
      
    };
   
    const result = controller.signup( req , res ); // error 500
    expect(result).to.be.equal(undefined);
  })

  it("12 - SIGNUP USER - Create NEW USER - SUCCESS - StatusCode 200", ()=> {

    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        phones: '+55988776655',
        roles: 'admin',
      },
      user: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        save: (process) => process(false, {
          roles: 'null',
          save: (process) => process(false, {roles: null}),
        }),
      },
      role: {
        find: (data, process) => process(false, [{_id:'0001'}]),
        findOne: (data, process) => process(false, {_id: '11223'}),
      },
      
    };
   
    const result = controller.signup( req , res ); // error 500
    expect(result).to.be.equal(undefined);
  })

  it("13 - SIGNUP USER - Create NEW USER - Error findOne - StatusCode 500", ()=> {

    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        phones: '+55988776655',
        roles: null,
      },
      user: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        save: (process) => process(false, {
          roles: null,
          save: (process) => process(false, {roles: null}),
        }),
      },
      role: {
        find: (data, process) => process(false, null),
        findOne: (data, process) => process(true, null),
      },
      
    };
   
    const result = controller.signup( req , res ); // error 500
    expect(result).to.be.equal(undefined);
  })

  it("14 - SIGNUP USER - Create NEW USER - Error saveUser - StatusCode 500", ()=> {

    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        phones: '+55988776655',
        roles: false,
      },
      user: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        save: (process) => process(false, {
          roles: null,
          save: (process) => process(true, {roles: null}),
        }),
      },
      role: {
        find: (data, process) => process(false, null),
        findOne: (data, process) => process(false, {_id: '11223'}),
      },
      
    };
   
    const result = controller.signup( req , res ); // error 500
    expect(result).to.be.equal(undefined);
  })

  it("15 - SIGNUP USER - Create NEW USER - Error on instancied User - StatusCode 500", ()=> {

    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
        phones: '+55988776655',
      },

      role: {
        find: (data, process) => process(false, null),
        findOne: (data, process) => process(false, {_id: '11223'}),
      },
      
    };
   
    const result = controller.signup( req , res ); // error 500
    expect(result).to.be.equal(undefined);
  })
})