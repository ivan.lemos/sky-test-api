const chai = require('chai')
const expect = chai.expect
const sinon = require('sinon'); 
 
const { Controller  } = require('../../app/controllers');
const { user  } = require('../../app/models');

describe("USER CONTROLLER", () => {

  beforeEach(() => {
    sinonSandBox = sinon.createSandbox();
  });

  afterEach(() => {
    // Restore all the things made through the sandbox
    sinonSandBox.restore();
  });
  
  
  it("1 - GET USER - FindOne error - statusCode 500", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    };
    sinonSandBox.stub(user, 'findOne').returns({
      populate : () => (
        {
          exec : (process) => {
            process('error test');
          } ,
        }),
    });
    const result = Controller.user.getUser( req , res );
    expect(result).to.be.equal(undefined);
  })

  it("2 - GET USER - User Not Found error - statusCode 404", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    };
    sinonSandBox.stub(user, 'findOne').returns({
      populate : () => (
        {
          exec : (process) => {
            process(false, false);
          } ,
        }),
    });
    const result = Controller.user.getUser( req , res );
    expect(result).to.be.equal(undefined);
  })

  it("3 - GET USER - User Not Found error - statusCode 404", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    };
    sinonSandBox.stub(user, 'findOne').returns({
      populate : () => (
        {
          exec : (process) => {
            process(false, {roles: [{name: 'admin'}]});
          } ,
        }),
    });
    const result = Controller.user.getUser( req , res );
    expect(result).to.be.equal(undefined);
  })

  
  it("4 - All Access - Public Content", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  null

    const result = Controller.user.allAccess( req , res );
    expect(result).to.be.equal(undefined);
  })

  it("5 - User Board - User Content", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  null

    const result = Controller.user.userBoard( req , res );
    expect(result).to.be.equal(undefined);
  })

  it("6 - Admin Board - Admin Content.", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  null

    const result = Controller.user.adminBoard( req , res );
    expect(result).to.be.equal(undefined);
  })

  it("7 - Moderator Board - Moderator Content.", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  null

    const result = Controller.user.moderatorBoard( req , res );
    expect(result).to.be.equal(undefined);
  })

})