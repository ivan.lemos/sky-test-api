const chai = require('chai')
const expect = chai.expect
const sinon = require('sinon'); 
 
const { Controller  } = require('../../app/controllers');


describe("INDEX CONTROLLER", () => {

  beforeEach(() => {
    sinonSandBox = sinon.createSandbox();
  });

  afterEach(() => {
    // Restore all the things made through the sandbox
    sinonSandBox.restore();
  });
  
  
  it("1 - CONTROLLERS EXPORT MODULES - Testings index controller ", ()=> {
    const control = Controller;
    expect(typeof control).to.be.equal('object');
  })
})