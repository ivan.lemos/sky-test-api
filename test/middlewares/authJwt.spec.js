const chai = require('chai')
const expect = chai.expect
const sinon = require('sinon'); 
const authJwt = require('../../app/middlewares/authJwt');
const { user  } = require('../../app/models');

describe("MIDDLEWARES - AUTH JW", () => {

  beforeEach(() => {
    sinonSandBox = sinon.createSandbox();
  });

  afterEach(() => {
    // Restore all the things made through the sandbox
    sinonSandBox.restore();
  });
  
  
  it("1 - IS ADMIN - Error findById - statusCode 500", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    };
    sinonSandBox.stub(user, 'findById').returns({
      exec : (process) => {
        process('error test');
      } ,
    });
    const result = authJwt.isAdmin( req , res );
    expect(result).to.be.equal(undefined);
  })

  it("2 - IS ADMIN - Error Role.find - statusCode 500", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => process('error'),
      },
    };
    const req2 =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    };
    sinonSandBox.stub(user, 'findById').returns({
      exec : (process) => {
        process(false, {roles:['name']});
      } ,
    });

    const result = authJwt.isAdmin( req , res );
    const result2 = authJwt.isAdmin( req2 , res );
    expect(result).to.be.equal(undefined);
    expect(result2).to.be.equal(undefined);
  })

  it("3 - IS ADMIN - Error Not is Admin- statusCode 403", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false,[{name: 'commum'}])},
      },
    };
    sinonSandBox.stub(user, 'findById').returns({
      exec : (process) => {
        process(false, {roles:['name']});
      } ,
    });

    const result = authJwt.isAdmin( req , res );
    expect(result).to.be.equal(undefined);
  })

  it("4 - IS ADMIN -  Error Not is Admin 2 - Empty array - statusCode 403", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false,[])},
      },
    };
    sinonSandBox.stub(user, 'findById').returns({
      exec : (process) => {
        process(false, {roles:['name']});
      } ,
    });

    const result = authJwt.isAdmin( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
  })

  it("5 - IS ADMIN - Success", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false,[{name: 'admin'}])},
      },
    };
    sinonSandBox.stub(user, 'findById').returns({
      exec : (process) => {
        process(false, {roles:['name']});
      } ,
    });

    const result = authJwt.isAdmin( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
  })

  it("6 - VERIFY TOKEN - Error No token provided! - statusCode 403", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      headers: JSON.parse(`
        {
          "x-access-token": null
        }
      `),
    };


    const result = authJwt.verifyToken( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
  })

  it("7 - VERIFY TOKEN - Error token Unauthorized! - statusCode 403", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      headers: JSON.parse(`
        {
          "x-access-token": "123"
        }
      `),
      jwt: {
        verify: (token, secret, process)=>{
          process('error');
        },
      },
    };
    const result = authJwt.verifyToken( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
  })

  it("8 - VERIFY TOKEN - Success", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      headers: JSON.parse(`
        {
          "x-access-token": "123"
        }
      `),
      jwt: {
        verify: (token, secret, process)=>{
          process(false, {id: '123456'});
        },
      },
    };
    const result = authJwt.verifyToken( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
  })

  it("9 - IS MODERADOR  - Success", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false,[{name: 'moderator'}])},
      },
    };
    sinonSandBox.stub(user, 'findById').returns({
      exec : (process) => {
        process(false, {roles:['name']});
      } ,
    });

    const result = authJwt.isModerator( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
  })

  it("10 - IS MODERADOR  - Error on findById", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false,[{name: 'moderator'}])},
      },
    };
    const req2 =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    };
    sinonSandBox.stub(user, 'findById').returns({
      exec : (process) => {
        process('error');
      } ,
    });
 

    const result = authJwt.isModerator( req , res, (()=>true ));
    const result2 = authJwt.isModerator( req2 , res, (()=>true ));
    expect(result).to.be.equal(undefined);
    expect(result2).to.be.equal(undefined);
  })

  it("11 - IS MODERADOR  - Error Role find", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process('error')},
      },
    };
    sinonSandBox.stub(user, 'findById').returns({
      exec : (process) => {
        process(false, {roles:['name']});
      } ,
    });

    const result = authJwt.isModerator( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
  })

  it("12 - IS MODERADOR  - Error Require Moderator Role!", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false, [] )},
      },
    };
    const req2 =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
    
    };
    const req3 =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false, [{name:'test'}] )},
      },
    
    };
    sinonSandBox.stub(user, 'findById').returns({
      exec : (process) => {
        process(false, {roles:['name']});
      } ,
    });

    const result = authJwt.isModerator( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
    const result2 = authJwt.isModerator( req2 , res, (()=>true ));
    expect(result2).to.be.equal(undefined);
    const result3= authJwt.isModerator( req3 , res, (()=>true ));
    expect(result3).to.be.equal(undefined);
  })

})