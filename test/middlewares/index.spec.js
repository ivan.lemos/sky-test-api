const chai = require('chai')
const expect = chai.expect
const sinon = require('sinon'); 
 
const { authJwt, verifySignUp  } = require('../../app/middlewares');


describe("MIDDLEWARES - INDEX", () => {

  beforeEach(() => {
    sinonSandBox = sinon.createSandbox();
  });

  afterEach(() => {
    // Restore all the things made through the sandbox
    sinonSandBox.restore();
  });
  
  
  it("1 - MIDDLEWARES EXPORT MODULES - Testing authJwt ", ()=> {
    const middlewares = authJwt;
    expect(typeof middlewares).to.be.equal('object');
  })

  it("2 - MIDDLEWARES EXPORT MODULES - Testing verifySignUp ", ()=> {
    const middlewares = verifySignUp;
    expect(typeof middlewares).to.be.equal('object');
  })
})