const chai = require('chai')
const expect = chai.expect
const sinon = require('sinon'); 
const verifySignUp  = require('../../app/middlewares/verifySignUp');
const { user  } = require('../../app/models');

describe("MIDDLEWARES - VERIFY SIGNUP", () => {

  beforeEach(() => {
    sinonSandBox = sinon.createSandbox();
  });

  afterEach(() => {
    // Restore all the things made through the sandbox
    sinonSandBox.restore();
  });
  
  
  it("1 - CHECK DUPLICATE USERNAME OR EMAIL - Success - User not found", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false,[{name: 'admin'}])},
      },
    };
    sinonSandBox.stub(user, 'findOne').returns({
      exec : (process) => {
        process(false, false);
      } ,
    });

    const result = verifySignUp.checkDuplicateUsernameOrEmail( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
  })

  it("2 - CHECK DUPLICATE USERNAME   - Error - Return generic error - statusCode 500", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false,[{name: 'admin'}])},
      },
    };
    sinonSandBox.stub(user, 'findOne').returns({
      exec : (process) => {
        process(true, false);
      } ,
    });

    const result = verifySignUp.checkDuplicateUsernameOrEmail( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
  })

  it("3 - CHECK DUPLICATE USERNAME  - Error - User found - statusCode 400", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false,[{name: 'admin'}])},
      },
    };
    sinonSandBox.stub(user, 'findOne').returns({
      exec : (process) => {
        process(null, {username: 'ivanlemos'});
      } ,
    });

    const result = verifySignUp.checkDuplicateUsernameOrEmail( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
  })

  it("4 - CHECK DUPLICATE MAIL - Error - User found- statusCode 400", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false,[{name: 'admin'}])},
      },
    };
    sinonSandBox.stub(user, 'findOne').returns({
      exec : (process) => {
        process(false, {username: 'admin'});
      } ,
    });
 
    const result = verifySignUp.checkDuplicateUsernameOrEmail( req , res, (()=>true ));
    expect(result).to.be.equal(undefined);
  })

  it("5 - CHECK DUPLICATE MAIL - Error - Email found - statusCode 400", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false,[{name: 'admin'}])},
      },
    };
    sinonSandBox.stub(user, 'findOne').returns({
      exec : (process) => {
        process(null, {email: 'ivan.lemos@accenture.com'});
      } ,
    });
 
    const result = verifySignUp.checkDuplicateUsernameOrEmail( req , res, ()=>true );
    expect(result).to.be.equal(undefined);
  })

  it("6 - CHECK ROLES EXISTED - Return Error if not exist - statusCode 400", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        roles:['test'],
      },
    };
    const req2 =  {
      body: {
        roles:['test'],
      },
    };
    const req3 =  {
      body: {
        roles:false,
      },
    };
   
 
    const result = verifySignUp.checkRolesExisted( req , res, (() => true ));
    const result2 = verifySignUp.checkRolesExisted( req2 , res, (() => true ));
    const result3 = verifySignUp.checkRolesExisted( req3 , res, (() => true ));
    expect(result).to.be.equal(undefined);
    expect(result2).to.be.equal(undefined);
    expect(result3).to.be.equal(undefined);
  })

  it("7 - CHECK ROLES EXISTED - Success - void()", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        roles:['admin'],
      },
    };
 
    const result = verifySignUp.checkRolesExisted( req , res, (() => true ));
    expect(result).to.be.equal(undefined);
  })

  it("8 - CHECK DUPLICATE MAIL - Test user undefined - statusCode 400", ()=> {
    var res = {
      status : (code) => ({   
        send: (data) => {
          this.message = data.message;
          this.statusCode = code;
        }, 
      }),
    };
    
    const req =  {
      body: {
        username:"ivanlemos",
        email:"ivan.lemos@accenture.com",
        password:"accenture@12345",
      },
      role:{
        find: (data, process) => {process(false,[{name: 'admin'}])},
      },
    };
    sinonSandBox.stub(user, 'findOne').returns({
      exec : (process) => {
        process(null, null);
      } ,
    });
 
    const result = verifySignUp.checkDuplicateUsernameOrEmail( req , res, ()=>true );
    expect(result).to.be.equal(undefined);
  })
})