const chai = require('chai')
const expect = chai.expect

const { secret } = require('../../app/config/auth.config');


describe("GET CONFIG AUTH", () => {

  it("1 - get secret", ()=> {
    expect(secret).to.be.a('string');
  })

})