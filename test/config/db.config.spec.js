const chai = require('chai')
const expect = chai.expect

const { DB, HOST } = require('../../app/config/db.config');


describe("GET CONFIG DB", () => {

  it("1 - get config DB from the file", ()=> {
    expect(DB).to.be.a('string');
  })

  it("2 - get config DB HOST from the file", ()=> {
    expect(HOST).to.be.a('string');
  })
})