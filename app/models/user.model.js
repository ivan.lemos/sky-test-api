const mongoose = require("mongoose");
const timestamps = require('mongoose-timestamp');


const user = new mongoose.Schema({
  name:String,
  phones: [
    {
      type: Object,
    },
  ],  
  username: String,
  email: String,
  password: String,
  roles: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Role",
    },
  ],
  lastLoginAt: Date,
});

user.plugin(timestamps);

const User = mongoose.model("User",user);

module.exports = User;