const _ = require('lodash');
const db = require("../models");
const ROLES = db.ROLES;
const User = db.user;

 
checkDuplicateUsernameOrEmail = (req, res, next) => {
  // Username
  const query  =  { $or: [{username: req.body.username }, {email: req.body.email }]};

  User.findOne(query).exec((err, user)=>{
    if(!_.isNull(err)){
      res.status(500).send({ message: 'error' });
      return 
    }else{
      if (!_.isUndefined(user) && !_.isNull(user)) {
        let message = null;

        if(req.body.username  == user.username ){ 
          message  = 'Username already exists';
        }
        if(req.body.email  == user.email ){ 
          message  = 'Email already exists';
        }
        res.status(400).send({ message: `Failed! already in use!.${message}.`  });
        return;
      } 
    }
    next();
  })
  
};

checkRolesExisted = (req, res, next) => {
  if (req.body.roles) {
    for (let i = 0; i < req.body.roles.length; i++) {
      if (!ROLES.includes(req.body.roles[i])) {
        res.status(400).send({
          message: `Failed! Role ${req.body.roles[i]} does not exist!`,
        });
        return;
      }
    }
  }

  next();
};

const verifySignUp = {
  checkDuplicateUsernameOrEmail,
  checkRolesExisted,
};

module.exports = verifySignUp;