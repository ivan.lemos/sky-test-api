const { secret } = require('../config/auth.config');
const { user , role  } = require('../models');
const { sign } = require('jsonwebtoken');
const { hashSync, compareSync } = require('bcryptjs');


/**
 * signup
 * Register user on GET /api/auth/signup
 * @param {*} req 
 * @param {*} res 
 */
const signup = (req, res) => {

  const Role = req.role  || role;
  const rootUser = req.user || new user({
    username: req.body.username,
    email: req.body.email,
    phones: req.body.phones,
    password: hashSync(req.body.password, 8),
  });


  rootUser.save((err, user) => {
    if(err){
      res.status(500).send({ message: 'error' });
      return;
    }
    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles },
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: 'error' });
            return;
          }
  
          user.roles = roles.map(role => role._id);
          
          user.save((err)=>{
            if (err) {
              res.status(500).send({ message: 'error' });
              return;
            }
            signin(req, res);
          })
        },
      );
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        user.roles = [role._id];
        user.save((err)=>{
          if (err) {
            res.status(500).send({ message: 'error' });
            return;
          }
          signin(req, res);
        })
  
      });
    }
  })
  return;
};

/**
 * signin
 * Make login user and receiver x-access-token on POST /api/auth/signin
 * @param {*} req 
 * @param {*} res 
 */
const signin = (req, res) => {
  const User = user;
  User.findOne({
    username: req.body.username,
  })
    .populate("roles", "-__v")
    .exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = compareSync(
        req.body.password,
        user.password,
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!",
        });
      }

      var token = sign({ id: user.id }, secret, {
        expiresIn: 1800, // 30 minutes
      });

      var authorities = [];

      for (let i = 0; i < user.roles.length; i++) {
        authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
      }

      // set lastlogin user
      user.lastLoginAt = new Date();
      // save last login and return to user 
      user.save(err => {
        
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        
        res.status(200).send({
          // eslint-disable-next-line no-underscore-dangle
          id: user._id,
          username: user.username,
          email: user.email,
          phones:user.phones,
          roles: authorities,
          createdAt:user.createdAt,
          updateAt:user.updateAt,
          lastLoginAt: user.lastLoginAt,
          accessToken: token,
        });
      });
    });
};


module.exports = { signup, signin };