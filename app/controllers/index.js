const auth = require("./auth.controller");
const user = require("./user.controller");
module.exports = {
  Controller : {
    auth,
    user,
  },
};  